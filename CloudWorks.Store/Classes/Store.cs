﻿using CloudWorks.Store.BaseClasses;
using CloudWorks.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CloudWorks.Store.Classes
{
    //Колекції в цьому класі використовуються для збереження груп елементів
    //порядок додавання та збереження елементів не важливий
    //доступ по індексу та пошук не потрібен
    //присутні оперяції об'єднання колекцій
    //List i LinkedList є рівноцінним вибором в даній ситуації
    //HasSet є хорошим варіантом для роботи з колекціями які об'єднуються, але тоді б ми були вимушені використовувати для
    //всіх колекцій HashSet з однаковим типом елемента (HashSet<IPrintable>) що б призвело постійного приведення типів
    //при роботі з елементами колекції.
    //Тому вибір зупинився на List як на найбільш звичній колекції враховуючи рівноцінність вибору
    public class Store : IPrintable
    {
        private List<StandardPos> _standardPoses;

        private List<SelfCheckoutPos> _selfCheckoutPoses;
        public List<Client> Clients { get; private set; }
        public List<Employee> Employees { get; private set; }

        public List<Pos> Poses
        {
            get
            {
                var result = new List<Pos>(this._selfCheckoutPoses);
                result.AddRange(this._standardPoses);
                return result;
            }
        }


        public Store(int selfCheckoutCount, Person[] persons)
        {
            this._selfCheckoutPoses = new List<SelfCheckoutPos>(selfCheckoutCount);
            for (int i = 0; i < selfCheckoutCount; i++)
                this._selfCheckoutPoses.Add(new SelfCheckoutPos(i + 1));

            this.Clients = (from person in persons
                            where person is Client
                            select (Client)person).ToList();
            this.Employees = (from person in persons
                              where person is Employee
                              select (Employee)person).ToList();


            this._standardPoses = new List<StandardPos>(Employees.Count);
            this._standardPoses.AddRange(from employee in this.Employees
                                         select new StandardPos(this.Employees.IndexOf(employee), employee));
        }

        public double Checkout()
        {
            double checkoutTime = 0;
            Random random = new Random();

            foreach (var client in this.Clients)
            {
                var posNumber = random.Next(0, this.Poses.Count);
                checkoutTime += this.Poses[posNumber].Checkout(client);
            }

            return checkoutTime;
        }
        public List<Person> GetPersons() => this.Clients.Cast<Person>().Concat(this.Employees).ToList();
        public void Print()
        {
            var result = new List<IPrintable>(this.Poses);
            result.AddRange(this.Clients);
            result.AddRange(this.Employees);

            foreach (var element in result)
                element.Print();
        }
    }
}
