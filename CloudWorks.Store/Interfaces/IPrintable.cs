﻿namespace CloudWorks.Store.Interfaces
{
    public interface IPrintable
    {
        void Print();
    }
}
