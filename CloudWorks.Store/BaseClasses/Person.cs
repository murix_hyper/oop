﻿using CloudWorks.Store.Interfaces;
using CloudWorks.Store.Attributes;
using System.Reflection;
using System;
namespace CloudWorks.Store.BaseClasses
{
    public abstract class Person : IPrintable
    {
        protected Person(string name, double checkoutSpeed)
        {
            if (name.Length > typeof(Person).GetProperty("Name")?.GetCustomAttribute<PersonLengthAttribute>()?._length)
                throw new Exception("A Person's name cannot be longer than allowed");
            Name = name;
            CheckoutSpeed = checkoutSpeed;
        }
        [PersonLength(30)]
        public string Name { get; private set; }
        public double CheckoutSpeed { get; private set; }
        public abstract void Print();
    }
}
