using System.Reflection;
namespace CloudWorks.Store.Attributes
{
    class PersonLengthAttribute : System.Attribute
    {
        public int _length { get; }
        public PersonLengthAttribute() { }
        public PersonLengthAttribute(int length) => _length = length;
    }
}